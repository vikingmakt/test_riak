PROJECT = test_riak
PROJECT_DESCRIPTION = test riak
PROJECT_REGISTERED = test_riak
PROJECT_VERSION = 0.1.0

DEPS = loki riakc
dep_loki = git https://gitlab.com/vikingmakt/loki.git f/rmq

include erlang.mk
